package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class PromotionTest {
    Piece[][] board = new Piece[8][8];

    @BeforeEach
    public void GenerateEmptyBoardForTesting() {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                board[i][j] =  new Empty(Piece.PieceType.EMPTY);
            }
        }
        //black king
        board[6][6] = new King(Piece.PieceType.KING,true, Team.BLACK, "g7", null, board);

        //white king
        board[0][0] = new King(Piece.PieceType.KING,true, Team.WHITE, "a1", null, board);

    }

//its necessary to comment GUI promotion in board and uncomment console chess promotion below before starting this test
    @Test
    public void Promotion_WhitePawnWillMove_WhitePawnWillPromoteToQueen() {

        Board b = new Board(board);
        b.setGameState(Team.WHITE);

        ByteArrayInputStream in = new ByteArrayInputStream("q".getBytes());
        System.setIn(in);


        //white pawn
        board[6][2] = new Pawn(Piece.PieceType.PAWN, false, Team.WHITE, "c7", null, board, false);

        Piece pawn = board[6][2];

        ArrayList<String> pawnAllowedMoves = pawn.allowedMoves();
        ArrayList<String> expectedPawnAllowedMoves = new ArrayList<>();
        expectedPawnAllowedMoves.add("c8");

        Assertions.assertEquals(expectedPawnAllowedMoves,pawnAllowedMoves);


        b.movePiece(pawn, "c8");


        String emptyCurrentPosition = board[6][2].getCurrentPosition();

        Assertions.assertEquals(null, emptyCurrentPosition);


        String newPawnPosition = board[7][2].getCurrentPosition();
        String newExpectedPawnPosition = "c8";

        Assertions.assertEquals(newExpectedPawnPosition, newPawnPosition);


        ArrayList<String> newQueenAllowedMoves = board[7][2].allowedMoves();
        ArrayList<String> expectedQueenAllowedMoves = new ArrayList<>(Arrays.asList("c7", "c6", "c5", "c4", "c3", "c2", "c1", "d8", "e8", "f8", "g8", "h8", "b8", "a8", "b7", "a6", "d7", "e6", "f5", "g4", "h3"));

        Assertions.assertEquals(expectedQueenAllowedMoves,newQueenAllowedMoves);
        Assertions.assertEquals(board[7][2].getPieceType(), Piece.PieceType.QUEEN);
        Assertions.assertEquals(board[7][2].getTeam(), Team.WHITE);

    }
}