package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import cz.cvut.fel.pjv.chess.view.App;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EnPassantTest {
    Piece[][] board = new Piece[8][8];

    @BeforeEach
    public void GenerateEmptyBoardForTesting() {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                board[i][j] =  new Empty(Piece.PieceType.EMPTY);
            }
        }

        //black king
        board[6][6] = new King(Piece.PieceType.KING,true, Team.BLACK, "g7", null, board);

        //white king
        board[0][0] = new King(Piece.PieceType.KING,true, Team.WHITE, "a1", null, board);

    }


    @Test
    public void EnPassant_TwoPawnsWillMove_BlackPawnWillBeCaptured() {

        Board b = new Board(board);
        b.setGameState(Team.BLACK);

        //black pawn
        board[6][2] = new Pawn(Piece.PieceType.PAWN, true, Team.BLACK, "c7", null, board, false);

        //white pawn
        board[4][3] = new Pawn(Piece.PieceType.PAWN, false, Team.WHITE, "d5", null, board, false);

        Piece whitePawn = board[4][3];
        Piece blackPawn = board[6][2];

        ArrayList<String> whitePawnAllowedMoves = whitePawn.allowedMoves();
        ArrayList<String> expectedWhitePawnAllowedMoves = new ArrayList<>();
        expectedWhitePawnAllowedMoves.add("d6");
        Assertions.assertEquals(expectedWhitePawnAllowedMoves,whitePawnAllowedMoves);

        ArrayList<String> blackPawnAllowedMoves = blackPawn.allowedMoves();
        ArrayList<String> expectedBlackPawnAllowedMoves = new ArrayList<>();
        expectedBlackPawnAllowedMoves.add("c6");
        expectedBlackPawnAllowedMoves.add("c5");
        Assertions.assertEquals(expectedBlackPawnAllowedMoves,blackPawnAllowedMoves);

        b.movePiece(blackPawn, "c5");

        boolean blackEnPassant = blackPawn.isEnPassantReady();
        Assertions.assertEquals(true, blackEnPassant);

        boolean whiteEnPassant = whitePawn.isEnPassantReady();
        Assertions.assertEquals(false, whiteEnPassant);

        b.movePiece(whitePawn, "c6");

        boolean blackEnPassant2 = blackPawn.isEnPassantReady();
        Assertions.assertEquals(true, blackEnPassant2);

        boolean whiteEnPassant2 = whitePawn.isEnPassantReady();
        Assertions.assertEquals(false, whiteEnPassant2);

        String emptyCurrentPosition = board[4][2].getCurrentPosition();
        Assertions.assertEquals(null, emptyCurrentPosition);

        String newWhitePawnPosition = whitePawn.getCurrentPosition();
        String newExpectedWhitePawnPosition = "c6";

        Assertions.assertEquals(newExpectedWhitePawnPosition, newWhitePawnPosition);

        boolean isWhiteInCheck = b.isWhiteInCheck();
        boolean isBlackInCheck = b.isBlackInCheck();
        Assertions.assertEquals(false, isWhiteInCheck);
        Assertions.assertEquals(false, isBlackInCheck);
    }
}