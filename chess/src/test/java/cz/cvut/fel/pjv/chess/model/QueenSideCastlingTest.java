package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.Empty;
import cz.cvut.fel.pjv.chess.model.pieces.King;
import cz.cvut.fel.pjv.chess.model.pieces.Piece;
import cz.cvut.fel.pjv.chess.model.pieces.Rook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class QueenSideCastlingTest {

        Piece[][] board = new Piece[8][8];

        @BeforeEach
        public void GenerateEmptyBoardForTesting() {
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    board[i][j] =  new Empty(Piece.PieceType.EMPTY);
                }
            }

        }


        @Test
        public void Castling_DoCastling_WhiteKingWillSwitchWithRook() {

            //black king
            board[6][0] = new King(Piece.PieceType.KING,true, Team.BLACK, "a7", null, board);

            //white king
            board[0][4] = new King(Piece.PieceType.KING,true, Team.WHITE, "e1", null, board);
            //white rook
            board[0][0] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "a1", null, board);


            Board b = new Board(board);
            b.setGameState(Team.WHITE);

            Piece whiteKing = board[0][4];

            ArrayList<String> whiteKingAllowedMoves = whiteKing.allowedMoves();
            ArrayList<String> expectedWhiteKingAllowedMoves = new ArrayList<>();
            expectedWhiteKingAllowedMoves.add("e2");
            expectedWhiteKingAllowedMoves.add("f1");
            expectedWhiteKingAllowedMoves.add("f2");
            expectedWhiteKingAllowedMoves.add("d1");
            expectedWhiteKingAllowedMoves.add("d2");

            Assertions.assertEquals(expectedWhiteKingAllowedMoves,whiteKingAllowedMoves);

            b.movePiece(whiteKing, "a1");

            String emptyCurrentPosition = board[0][4].getCurrentPosition();
            Assertions.assertNull(board[0][4].allowedMoves());
            Assertions.assertNull(board[0][4].getTeam());
            Assertions.assertFalse(board[0][4].isEnPassantReady());
            Assertions.assertFalse(board[0][4].isFirstMove());
            Assertions.assertNull(emptyCurrentPosition);

            String emptyCurrentPosition2 = board[0][0].getCurrentPosition();
            Assertions.assertNull(board[0][0].allowedMoves());
            Assertions.assertNull(board[0][0].getTeam());
            Assertions.assertFalse(board[0][0].isEnPassantReady());
            Assertions.assertFalse(board[0][0].isFirstMove());
            Assertions.assertNull(emptyCurrentPosition2);

            String emptyCurrentPosition3 = board[0][1].getCurrentPosition();
            Assertions.assertNull(board[0][1].allowedMoves());
            Assertions.assertNull(board[0][1].getTeam());
            Assertions.assertFalse(board[0][1].isEnPassantReady());
            Assertions.assertFalse(board[0][1].isFirstMove());
            Assertions.assertNull(emptyCurrentPosition3);


            String newWhiteKingPosition = board[0][2].getCurrentPosition();
            String newExpectedWhiteKingPosition = "c1";

            Assertions.assertEquals(newExpectedWhiteKingPosition, newWhiteKingPosition);


            String newWhiteRookPosition = board[0][3].getCurrentPosition();
            String newExpectedWhiteRookPosition = "d1";

            Assertions.assertEquals(newExpectedWhiteRookPosition, newWhiteRookPosition);


            boolean isWhiteInCheck = b.whiteInCheck;
            boolean isBlackInCheck = b.blackInCheck;

            Assertions.assertFalse(isBlackInCheck);
            Assertions.assertFalse(isWhiteInCheck);
    }


    @Test
    public void Castling_DoCastling_BlackKingWillSwitchWithRook() {

        //white king
        board[6][0] = new King(Piece.PieceType.KING,true, Team.WHITE, "a7", null, board);

        //black king
        board[0][4] = new King(Piece.PieceType.KING,true, Team.BLACK, "e1", null, board);
        //black rook
        board[0][0] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "a1", null, board);


        Board b = new Board(board);
        b.setGameState(Team.BLACK);

        Piece blackKing = board[0][4];

        ArrayList<String> blackKingAllowedMoves = blackKing.allowedMoves();
        ArrayList<String> expectedBlackKingAllowedMoves = new ArrayList<>();
        expectedBlackKingAllowedMoves.add("e2");
        expectedBlackKingAllowedMoves.add("f1");
        expectedBlackKingAllowedMoves.add("f2");
        expectedBlackKingAllowedMoves.add("d1");
        expectedBlackKingAllowedMoves.add("d2");

        Assertions.assertEquals(expectedBlackKingAllowedMoves,blackKingAllowedMoves);


        b.movePiece(blackKing, "a1");

        String emptyCurrentPosition = board[0][4].getCurrentPosition();
        Assertions.assertNull(board[0][4].allowedMoves());
        Assertions.assertNull(board[0][4].getTeam());
        Assertions.assertFalse(board[0][4].isEnPassantReady());
        Assertions.assertFalse(board[0][4].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition);

        String emptyCurrentPosition2 = board[0][0].getCurrentPosition();
        Assertions.assertNull(board[0][0].allowedMoves());
        Assertions.assertNull(board[0][0].getTeam());
        Assertions.assertFalse(board[0][0].isEnPassantReady());
        Assertions.assertFalse(board[0][0].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition2);

        String emptyCurrentPosition3 = board[0][1].getCurrentPosition();
        Assertions.assertNull(board[0][1].allowedMoves());
        Assertions.assertNull(board[0][1].getTeam());
        Assertions.assertFalse(board[0][1].isEnPassantReady());
        Assertions.assertFalse(board[0][1].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition3);


        String newBlackKingPosition = board[0][2].getCurrentPosition();
        String newExpectedBlackKingPosition = "c1";

        Assertions.assertEquals(newExpectedBlackKingPosition, newBlackKingPosition);


        String newBlackRookPosition = board[0][3].getCurrentPosition();
        String newExpectedBlackRookPosition = "d1";

        Assertions.assertEquals(newExpectedBlackRookPosition, newBlackRookPosition);


        boolean isWhiteInCheck = b.whiteInCheck;
        boolean isBlackInCheck = b.blackInCheck;

        Assertions.assertFalse(isBlackInCheck);
        Assertions.assertFalse(isWhiteInCheck);
    }
}
