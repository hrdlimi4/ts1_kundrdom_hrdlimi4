package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


public class KingSideCastlingTest {
    Piece[][] board = new Piece[8][8];

    @BeforeEach
    public void GenerateEmptyBoardForTesting() {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                board[i][j] =  new Empty(Piece.PieceType.EMPTY);
            }
        }

    }


    @Test
    public void Castling_DoCastling_BlackKingWillSwitchWithRook() {

        //white king
        board[6][0] = new King(Piece.PieceType.KING,true, Team.WHITE, "a7", null, board);

        //black king
        board[0][4] = new King(Piece.PieceType.KING,true, Team.BLACK, "e1", null, board);
        //black rook
        board[0][7] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "h1", null, board);


        Board b = new Board(board);
        b.setGameState(Team.BLACK);

        Piece blackKing = board[0][4];

        ArrayList<String> blackKingAllowedMoves = blackKing.allowedMoves();
        ArrayList<String> expectedBlackKingAllowedMoves = new ArrayList<>();
        expectedBlackKingAllowedMoves.add("e2");
        expectedBlackKingAllowedMoves.add("f1");
        expectedBlackKingAllowedMoves.add("f2");
        expectedBlackKingAllowedMoves.add("d1");
        expectedBlackKingAllowedMoves.add("d2");

        Assertions.assertEquals(expectedBlackKingAllowedMoves,blackKingAllowedMoves);


        b.movePiece(blackKing, "h1");


        String emptyCurrentPosition = board[0][4].getCurrentPosition();
        Assertions.assertNull(board[0][4].allowedMoves());
        Assertions.assertNull(board[0][4].getTeam());
        Assertions.assertFalse(board[0][4].isEnPassantReady());
        Assertions.assertFalse(board[0][4].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition);

        String emptyCurrentPosition2 = board[0][7].getCurrentPosition();
        Assertions.assertNull(board[0][7].allowedMoves());
        Assertions.assertNull(board[0][7].getTeam());
        Assertions.assertFalse(board[0][7].isEnPassantReady());
        Assertions.assertFalse(board[0][7].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition2);


        String newBlackKingPosition = board[0][6].getCurrentPosition();
        String newExpectedBlackKingPosition = "g1";

        Assertions.assertEquals(newExpectedBlackKingPosition, newBlackKingPosition);


        String newBlackRookPosition = board[0][5].getCurrentPosition();
        String newExpectedBlackRookPosition = "f1";

        Assertions.assertEquals(newExpectedBlackRookPosition, newBlackRookPosition);


        boolean isWhiteInCheck = b.whiteInCheck;
        boolean isBlackInCheck = b.blackInCheck;

        Assertions.assertFalse(isBlackInCheck);
        Assertions.assertFalse(isWhiteInCheck);
    }

    @Test
    public void Castling_DoCastling_WhiteKingWillSwitchWithRook() {

        //black king
        board[6][0] = new King(Piece.PieceType.KING,true, Team.BLACK, "a7", null, board);

        //white king
        board[0][4] = new King(Piece.PieceType.KING,true, Team.WHITE, "e1", null, board);
        //white rook
        board[0][7] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "h1", null, board);


        Board b = new Board(board);
        b.setGameState(Team.WHITE);

        Piece whiteKing = board[0][4];

        ArrayList<String> whiteKingAllowedMoves = whiteKing.allowedMoves();
        ArrayList<String> expectedWhiteKingAllowedMoves = new ArrayList<>();
        expectedWhiteKingAllowedMoves.add("e2");
        expectedWhiteKingAllowedMoves.add("f1");
        expectedWhiteKingAllowedMoves.add("f2");
        expectedWhiteKingAllowedMoves.add("d1");
        expectedWhiteKingAllowedMoves.add("d2");

        Assertions.assertEquals(expectedWhiteKingAllowedMoves,whiteKingAllowedMoves);


        b.movePiece(whiteKing, "h1");


        String emptyCurrentPosition = board[0][4].getCurrentPosition();
        Assertions.assertNull(board[0][4].allowedMoves());
        Assertions.assertNull(board[0][4].getTeam());
        Assertions.assertFalse(board[0][4].isEnPassantReady());
        Assertions.assertFalse(board[0][4].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition);

        String emptyCurrentPosition2 = board[0][7].getCurrentPosition();
        Assertions.assertNull(board[0][7].allowedMoves());
        Assertions.assertNull(board[0][7].getTeam());
        Assertions.assertFalse(board[0][7].isEnPassantReady());
        Assertions.assertFalse(board[0][7].isFirstMove());
        Assertions.assertNull(emptyCurrentPosition2);


        String newWhiteKingPosition = board[0][6].getCurrentPosition();
        String newExpectedWhiteKingPosition = "g1";

        Assertions.assertEquals(newExpectedWhiteKingPosition, newWhiteKingPosition);


        String newWhiteRookPosition = board[0][5].getCurrentPosition();
        String newExpectedWhiteRookPosition = "f1";

        Assertions.assertEquals(newExpectedWhiteRookPosition, newWhiteRookPosition);


        boolean isWhiteInCheck = b.whiteInCheck;
        boolean isBlackInCheck = b.blackInCheck;

        Assertions.assertFalse(isBlackInCheck);
        Assertions.assertFalse(isWhiteInCheck);
    }
}