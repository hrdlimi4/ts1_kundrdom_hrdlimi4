package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class BlackInCheckTest {
    Piece[][] board = new Piece[8][8];

    @BeforeEach
    public void gBoardForTesting() {
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                board[i][j] =  new Empty(Piece.PieceType.EMPTY);
            }
        }
    }

    @Test
    public void blackInCheck_WhitePieceWillMove_BlackKingWillGetInCheck() {
        Board b = new Board(board);
        b.setGameState(Team.WHITE);

        //black king
        board[4][4] = new King(Piece.PieceType.KING, true, Team.BLACK, "e5", null, board);
        Assertions.assertEquals(board[4][4].getPieceType(), Piece.PieceType.KING);

        //white knight
        board[7][1] = new Knight(Piece.PieceType.KNIGHT, true, Team.WHITE, "b8", null, board);
        Assertions.assertEquals(board[7][1].getPieceType(), Piece.PieceType.KNIGHT);

        ArrayList<String> knightAllowedMoves = board[7][1].allowedMoves();
        ArrayList<String> expectedKnightAllowedMoves = new ArrayList<>(Arrays.asList("c6","d7","a6"));
        Assertions.assertEquals(expectedKnightAllowedMoves,knightAllowedMoves);

        Piece knight = board[7][1];
        b.movePiece(knight, "c6");

        String emptyCurrentPosition = board[7][1].getCurrentPosition();
        Assertions.assertNull(emptyCurrentPosition);

        String newKnightPosition = board[5][2].getCurrentPosition();
        String newExpectedKnightPosition = "c6";
        Assertions.assertEquals(newExpectedKnightPosition, newKnightPosition);

        ArrayList<String> newKnightAllowedMoves = board[5][2].allowedMoves();
        ArrayList<String> newExpectedKnightAllowedMoves = new ArrayList<>(Arrays.asList("d8","d4","e7","e5","b8","b4","a7","a5"));
        Assertions.assertEquals(newExpectedKnightAllowedMoves,newKnightAllowedMoves);

        boolean isWhiteInCheck = b.isWhiteInCheck();
        boolean isBlackInCheck = b.isBlackInCheck();

        Assertions.assertTrue(isBlackInCheck);
        Assertions.assertFalse(isWhiteInCheck);
    }
}