package cz.cvut.fel.pjv.chess.model.pieces;


import cz.cvut.fel.pjv.chess.model.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;


class PawnTest {
        Piece[][] board = new Piece[8][8];

        @BeforeEach
        public void SetDefaultBoardForTesting() {
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    board[i][j] =  new Empty(Piece.PieceType.EMPTY);
                }
            }
            //black
            //pawn
            char letter = 'a';
            for (int j = 0; j < 8; j++) {
                String currentPos = letter + "2";
                letter++;
                board[1][j] = new Pawn(Piece.PieceType.PAWN, true, Team.WHITE, currentPos, null,board, false);
            }
            //rook
            board[0][0] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "a1", null, board);
            board[0][7] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "h1", null, board);

            //knight
            board[0][1] = new Knight(Piece.PieceType.KNIGHT,true, Team.WHITE, "b1", null, board);
            board[0][6] = new Knight(Piece.PieceType.KNIGHT,true, Team.WHITE, "g1", null, board);

            //bishop
            board[0][2] = new Bishop(Piece.PieceType.BISHOP,true, Team.WHITE, "c1", null, board);
            board[0][5] = new Bishop(Piece.PieceType.BISHOP,true, Team.WHITE, "f1", null, board);

            //king
            board[0][4] = new King(Piece.PieceType.KING,true, Team.WHITE, "e1", null, board);

            //queen
            board[0][3] = new Queen(Piece.PieceType.QUEEN,true, Team.WHITE, "d1", null, board);

            //white
            //pawn
            letter = 'a';
            for (int j = 0; j < 8; j++) {
                String currentPos = letter + "7";
                letter++;
                board[6][j] = new Pawn(Piece.PieceType.PAWN, true, Team.BLACK, currentPos, null, board, false);
            }

            //rook
            board[7][0] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "a8", null, board);
            board[7][7] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "h8", null, board);


            //knight
            board[7][1] = new Knight(Piece.PieceType.KNIGHT,true, Team.BLACK, "b8", null, board);
            board[7][6] = new Knight(Piece.PieceType.KNIGHT,true, Team.BLACK, "g8", null, board);

            //bishop
            board[7][2] = new Bishop(Piece.PieceType.BISHOP,true, Team.BLACK, "c8", null, board);
            board[7][5] = new Bishop(Piece.PieceType.BISHOP,true, Team.BLACK, "f8", null, board);

            //king
            board[7][4] = new King(Piece.PieceType.KING,true, Team.BLACK, "e8", null, board);

            //queen
            board[7][3] = new Queen(Piece.PieceType.QUEEN,true, Team.BLACK, "d8", null, board);
        }

        @Test
        public void CheckPossibleMoves_WhitePawnMovesFromD4_WhitePawnHas1CheckAccessibleMove() {
            //modify board
            board[3][3] = new Pawn(Piece.PieceType.PAWN,false, Team.WHITE, "d4", null, board, false);

            ArrayList<String> result = board[3][3].allowedMoves();

            Pawn mockPawn = mock(Pawn.class);
            board[3][3] = mockPawn;

            when(mockPawn.allowedMoves()).thenReturn(new ArrayList<>(Arrays.asList("d5")));

            ArrayList<String> expectedResult = board[3][3].allowedMoves();

            verify(mockPawn).allowedMoves();

            Collections.sort(result);
            Collections.sort(expectedResult);
            Assertions.assertEquals(expectedResult,result);
        }

        @Test
        public void CheckPossibleMoves_BlackPawnMovesFromD4_BlackPawnHas1CheckAccessibleMove() {
            //modify board
            board[3][3] = new Pawn(Piece.PieceType.PAWN,false, Team.BLACK, "d4", null, board, false);

            ArrayList<String> result = board[3][3].allowedMoves();

            Pawn mockPawn = mock(Pawn.class);
            board[3][3] = mockPawn;

            when(mockPawn.allowedMoves()).thenReturn(new ArrayList<>(Arrays.asList("d3")));

            ArrayList<String> expectedResult = board[3][3].allowedMoves();

            verify(mockPawn).allowedMoves();

            Collections.sort(result);
            Collections.sort(expectedResult);
            Assertions.assertEquals(expectedResult,result);
        }

    @Test
    public void CheckPossibleMoves_WhitePawnMovesFromD4_WhitePawnHas2CheckAccessibleMoves() {
        //modify board
        board[3][3] = new Pawn(Piece.PieceType.PAWN,true, Team.WHITE, "d4", null, board, false);

        ArrayList<String> result = board[3][3].allowedMoves();

        Pawn mockPawn = mock(Pawn.class);
        board[3][3] = mockPawn;

        when(mockPawn.allowedMoves()).thenReturn(new ArrayList<>(Arrays.asList("d5","d6")));

        ArrayList<String> expectedResult = board[3][3].allowedMoves();

        verify(mockPawn).allowedMoves();

        Collections.sort(result);
        Collections.sort(expectedResult);
        Assertions.assertEquals(expectedResult,result);
    }

    @Test
    public void CheckPossibleMoves_BlackPawnMovesFromD5_BlackPawnHas2CheckAccessibleMoves() {
        //modify board
        board[4][3] = new Pawn(Piece.PieceType.PAWN,true, Team.BLACK, "d5", null, board, false);

        ArrayList<String> result = board[4][3].allowedMoves();

        Pawn mockPawn = mock(Pawn.class);
        board[4][3] = mockPawn;

        when(mockPawn.allowedMoves()).thenReturn(new ArrayList<>(Arrays.asList("d4", "d3")));

        ArrayList<String> expectedResult = board[4][3].allowedMoves();

        verify(mockPawn).allowedMoves();

        Collections.sort(result);
        Collections.sort(expectedResult);
        Assertions.assertEquals(expectedResult,result);
    }
}





//    Piece[][]
//    Board b = new Board(null);
//    Piece[][] board = b.setUpBoard();
//    Piece pawn = board[1][0];
//    ArrayList<String> pawnAllowedMoves = pawn.allowedMoves();

//
//    @Test
//    void allowedMoves() {
//        InputTransfer inputTransfer = mock(InputTransfer.class);
//        Piece[][] board = mock(Piece.class);
//        Pawn pawn = new Pawn(...);

        //ArrayList<String> expectedAllowedMoves = new ArrayList<>();
        //expectedAllowedMoves.add("a3");
        //expectedAllowedMoves.add("a4");
//
//        pawn.allowedMoves();
//
//        Assertions.assertEquals(expectedAllowedMoves, pawnAllowedMoves);
//    }