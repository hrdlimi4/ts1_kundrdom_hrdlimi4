package cz.cvut.fel.pjv.chess.model;

public class ClockTest {
    public static void main(String[] args) {
        Clock clock = new Clock();

        Thread observerThread = new Thread(() -> {
            while (true)
            {
                System.out.print("\rTime: "+ clock.getTime());
                sleep(10);
            }

        });
        observerThread.start();


        System.out.println("\nStarting Stopwatch: ");
        clock.start();
        sleep(2000);
        clock.stop();
        System.out.println("\nStopwatch Stopped. ");

        sleep(2000);

        System.out.println("\nStarting Stopwatch: ");
        clock.start();
        sleep(2000);
        clock.stop();
        System.out.println("\nStopwatch Stopped. ");

    }

    public static void sleep(int time)
    {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
