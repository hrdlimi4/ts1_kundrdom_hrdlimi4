package cz.cvut.fel.pjv.chess.model;

public enum Team {
    BLACK, WHITE;
}
