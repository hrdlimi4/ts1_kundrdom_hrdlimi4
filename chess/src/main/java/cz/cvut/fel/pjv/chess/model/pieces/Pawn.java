package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public class Pawn extends Piece {

    public Pawn(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board, boolean enPassantReady) {
        super(pieceType, isFirstMove, team, currentPosition, allowedMoves, board, enPassantReady);
    }

    @Override
    public ArrayList<String> allowedMoves() {
        ArrayList<String> allowedMoves = new ArrayList<>();
        InputTransfer position = new InputTransfer();

        char letter = currentPosition.charAt(0);
        char tmp = currentPosition.charAt(1);
        int num = Character.getNumericValue(tmp);
        // white side
        if (getTeam() == Team.WHITE) {
            potentialPosition = letter + String.valueOf(num + 1);
            if (num + 1 <= BOARD_SIZE && position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                allowedMoves.add(potentialPosition);
            }
            if (isFirstMove() && position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                potentialPosition = letter + String.valueOf(num + 2);
                if (position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                    allowedMoves.add(potentialPosition);
                }
            }

            letter++;
            if (num + 1 <= BOARD_SIZE && letter != 'i') {
                potentialPosition = letter + String.valueOf(num + 1);
                if (position.transfer(board, potentialPosition).pieceType != PieceType.EMPTY && this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
            letter -= 2;
            if (num + 1 <= BOARD_SIZE && letter != '`') {
                potentialPosition = letter + String.valueOf(num + 1);
                if (position.transfer(board, potentialPosition).pieceType != PieceType.EMPTY && this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
            // black side
        } else {
            letter = currentPosition.charAt(0);
            potentialPosition = letter + String.valueOf(num - 1);
            if (num - 1 >= 1 && position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                allowedMoves.add(potentialPosition);
            }

            if (isFirstMove() && position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                potentialPosition = letter + String.valueOf(num - 2);
                if (position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY) {
                    allowedMoves.add(potentialPosition);
                }
            }

            letter++;
            if (num - 1 >= 1 && letter != 'i') {
                potentialPosition = letter + String.valueOf(num - 1);
                if (position.transfer(board, potentialPosition).pieceType != PieceType.EMPTY && this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
            letter -= 2;
            if (num - 1 >= 1 && letter != '`') {
                potentialPosition = letter + String.valueOf(num - 1);
                if (position.transfer(board, potentialPosition).pieceType != PieceType.EMPTY && this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
        }
        return allowedMoves;
    }

    @Override
    public String getPieceChar() {
        return "";
    }

    @Override
    public String toString() {
        if (team == Team.WHITE) {
            return "\u2659" + "\u2009";

        } else {
            return "\u265F" + "\u2009";

        }
    }
}
