package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public class Bishop extends Piece {

    public Bishop(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board) {
        super(pieceType, isFirstMove, team, currentPosition, allowedMoves, board);
    }

    @Override
    public ArrayList<String> allowedMoves() {
        ArrayList<String> allowedMoves = new ArrayList<>();
        InputTransfer position = new InputTransfer();

        char letter = currentPosition.charAt(0);
        char tmp = currentPosition.charAt(1);
        int num = Character.getNumericValue(tmp);

        while (letter != 'h' && num != 8) {
            letter++;
            num++;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        letter = currentPosition.charAt(0);
        tmp = currentPosition.charAt(1);
        num = Character.getNumericValue(tmp);

        while (letter != 'a' && num != 8) {
            letter--;
            num++;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        letter = currentPosition.charAt(0);
        tmp = currentPosition.charAt(1);
        num = Character.getNumericValue(tmp);

        while (letter != 'a' && num != 1) {
            letter--;
            num--;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        letter = currentPosition.charAt(0);
        tmp = currentPosition.charAt(1);
        num = Character.getNumericValue(tmp);

        while (letter != 'h' && num != 1) {
            letter++;
            num--;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }
        return allowedMoves;
    }

    @Override
    public String getPieceChar() {
        return "B";
    }

    @Override
    public String toString() {
        if (team == Team.WHITE) {
            return "\u2009" + "\u2657";

        } else {
            return "\u2009" + "\u265D";

        }
    }
}

