package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public abstract class Piece {
    PieceType pieceType;
    boolean isFirstMove;
    Team team;
    String currentPosition;
    public String potentialPosition;
    public final int BOARD_SIZE = 8;
    ArrayList<String> allowedMoves;
    Piece[][] board;
    boolean enPassantReady;

    public Piece(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board) {
        this.pieceType = pieceType;
        this.isFirstMove = isFirstMove;
        this.team = team;
        this.currentPosition = currentPosition;
        this.allowedMoves = allowedMoves;
        this.board = board;
    }

    public Piece(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board, boolean enPassantReady) {
        this.pieceType = pieceType;
        this.isFirstMove = isFirstMove;
        this.team = team;
        this.currentPosition = currentPosition;
        this.allowedMoves = allowedMoves;
        this.board = board;
        this.enPassantReady = enPassantReady;
    }

    public Piece(PieceType pieceType) {
        this.pieceType = pieceType;
    }

    public Team getTeam() {
        return team;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public boolean isFirstMove() {
        return isFirstMove;
    }

    public String getCurrentPosition() {
        return currentPosition;
    }

    public ArrayList<String> getAllowedMoves() {
        return allowedMoves;
    }

    public void setAllowedMoves(ArrayList<String> allowedMoves) {
        this.allowedMoves = allowedMoves;
    }

    public abstract ArrayList<String> allowedMoves();

    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    public void setFirstMove(boolean firstMove) {
        isFirstMove = firstMove;
    }

    public boolean isEnPassantReady() {
        return enPassantReady;
    }

    public void setEnPassantReady(boolean enPassantReady) {
        this.enPassantReady = enPassantReady;
    }

    public String getImage() {
        String imagePath = this.team + "_" + this.pieceType + ".png";
        return imagePath;
    }

    public String getPieceChar(){return null;}

    public enum PieceType {
        EMPTY(0, "E"),
        PAWN(1, "P"),
        KNIGHT(3, "N"),
        BISHOP(3, "B"),
        ROOK(5, "R"),
        QUEEN(9, "Q"),
        KING(10, "K");

        private final int value;
        private final String pieceName;


        PieceType(int value, String pieceName) {
            this.value = value;
            this.pieceName = pieceName;
        }
    }
}

