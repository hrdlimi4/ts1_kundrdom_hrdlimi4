package cz.cvut.fel.pjv.chess.model.board;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Utils;
import cz.cvut.fel.pjv.chess.model.Team;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import cz.cvut.fel.pjv.chess.view.App;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

public class Board {
    Piece[][] board;
    boolean isMoved = false;
    String historyOfGame = "";
    int order = 0;
    App app;
    ArrayList<Piece> capturedPiecesWhite = new ArrayList<>();
    ArrayList<Piece> capturedPiecesBlack = new ArrayList<>();

    public Board(App app) {
        this.app = app;
    }

    public Board(Piece[][] board) {
        this.board = board;
    }

    final static Logger logger = Logger.getLogger(Board.class.getName());

    //generates board 8x8
    public Piece[][] generateEmptyBoard() {
        return new Piece[8][8];
    }

    //prepares a game based on chess rules
    public Piece[][] setUpBoard() {
        board = generateEmptyBoard();

        //black
        //pawn
        char letter = 'a';
        for (int j = 0; j < 8; j++) {
            String currentPos = letter + "2";
            letter++;
            board[1][j] = new Pawn(Piece.PieceType.PAWN, true, Team.WHITE, currentPos, null, board, false);
        }

        //rook
        board[0][0] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "a1", null, board);
        board[0][7] = new Rook(Piece.PieceType.ROOK,true, Team.WHITE, "h1", null, board);

        //knight
        board[0][1] = new Knight(Piece.PieceType.KNIGHT,true, Team.WHITE, "b1", null, board);
        board[0][6] = new Knight(Piece.PieceType.KNIGHT,true, Team.WHITE, "g1", null, board);

        //bishop
        board[0][2] = new Bishop(Piece.PieceType.BISHOP,true, Team.WHITE, "c1", null, board);
        board[0][5] = new Bishop(Piece.PieceType.BISHOP,true, Team.WHITE, "f1", null, board);

        //king
        board[0][4] = new King(Piece.PieceType.KING,true, Team.WHITE, "e1", null, board);

        //queen
        board[0][3] = new Queen(Piece.PieceType.QUEEN,true, Team.WHITE, "d1", null, board);

        //white
        //pawn
        letter = 'a';
        for (int j = 0; j < 8; j++) {
            String currentPos = letter + "7";
            letter++;
            board[6][j] = new Pawn(Piece.PieceType.PAWN, true, Team.BLACK, currentPos, null, board, false);
        }

        //rook
        board[7][0] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "a8", null, board);
        board[7][7] = new Rook(Piece.PieceType.ROOK,true, Team.BLACK, "h8", null, board);

        //knight
        board[7][1] = new Knight(Piece.PieceType.KNIGHT,true, Team.BLACK, "b8", null, board);
        board[7][6] = new Knight(Piece.PieceType.KNIGHT,true, Team.BLACK, "g8", null, board);

        //bishop
        board[7][2] = new Bishop(Piece.PieceType.BISHOP,true, Team.BLACK, "c8", null, board);
        board[7][5] = new Bishop(Piece.PieceType.BISHOP,true, Team.BLACK, "f8", null, board);


        //king
        board[7][4] = new King(Piece.PieceType.KING,true, Team.BLACK, "e8", null, board);


        //queen
        board[7][3] = new Queen(Piece.PieceType.QUEEN,true, Team.BLACK, "d8", null, board);


        //empty
        for (int i = 2; i < 6; i++) {
            for (int j = 0; j < 8; j++){
                board[i][j] = new Empty(Piece.PieceType.EMPTY);
            }
        }

        // board[4][3] = new King(Piece.PieceType.KING,true, Team.WHITE, "d5", null, board);

        // print board to see the game
        printBoard();
        return board;
    }



    //prints empty board with letters and numbers
    public void printBoard() {
        System.out.println("  A  B  C  D  E  F  G  H");
        int x = 8;
        for (int i = 7; i >= 0; i--) {
            System.out.print(x + " ");
            x--;
            for (int j = 0; j < 8; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("  A  B  C  D  E  F  G  H");
    }

    //whether is black or white turn
    private Team gameState = GAME_STATE_WHITE;
    static final Team GAME_STATE_WHITE = Team.WHITE;
    static final Team GAME_STATE_BLACK = Team.BLACK;

    public void changeGameState() {
        if (gameState == GAME_STATE_WHITE) {
            gameState = GAME_STATE_BLACK;
        } else {
            gameState = GAME_STATE_WHITE;
        }
    }
    public Team getGameState() {
        return this.gameState;
    }

    //whether is game still active
    private boolean gameActive = true;

    public boolean isGameActive() {
        return gameActive;
    }

    public void setGameActive(boolean gameActive) {
        this.gameActive = gameActive;
    }

    public String getHistoryOfGame() {
        return historyOfGame;
    }

    //whether is somebody in check - opponent's king is under threat of being captured
    public boolean whiteInCheck;

    public boolean blackInCheck;


    public void willGetOpponentInCheck(Piece piece, String potentialPosition) {
        Piece temp;
        ArrayList<String> allowedMoves;
        String originalPosition = piece.getCurrentPosition();
        piece.setCurrentPosition(potentialPosition);
        Team opponent;
        if (getGameState() == GAME_STATE_BLACK) {
            opponent = GAME_STATE_WHITE;
        } else {
            opponent = GAME_STATE_BLACK;
        }
        Piece king = findKing(opponent);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (getGameState() != opponent) {
                    temp = board[i][j];
                    if (temp.getPieceType() != Piece.PieceType.EMPTY) {
                        allowedMoves = temp.allowedMoves();
                        if (allowedMoves.contains(king.getCurrentPosition())) {
                            if (getGameState() == Team.BLACK) {
                                whiteInCheck = true;
                                logger.info("white in check");
                            } else {
                                blackInCheck = true;
                                logger.info("black in check");
                            }
                        }
                    }
                }
            }
        }
        piece.setCurrentPosition(originalPosition);
    }

    public void setGameState(Team gameState) {
        this.gameState = gameState;
    }

    //    public boolean willGetYourselfInCheck (Piece piece, String potentialPosition) {
//        Utils utils = new Utils();
//        Piece temp;
//        ArrayList<String> allowedMoves;
//        String ogPosition = piece.getCurrentPosition();
//        int xPiece = utils.getX(piece.getCurrentPosition());
//        int yPiece = utils.getY(piece.getCurrentPosition());
//        int xEmpty = utils.getX(potentialPosition);
//        int yEmpty = utils.getY(potentialPosition);
//
//        //move simulation
//        piece.setCurrentPosition(potentialPosition);
//        board[xPiece][yPiece] = new Empty(Piece.PieceType.EMPTY);
//        board[xEmpty][yEmpty] = piece;
//        Team currentPlayer = getGameState();
//        Piece king = findKing(currentPlayer);
//
//        //go through the board and find out whether king will get in check
//        for (int i = 0; i < 8; i++) {
//            for (int j = 0; j < 8; j++) {
//                temp = board[i][j];
//                if (temp.getPieceType() != Piece.PieceType.EMPTY) {
//                    if (temp.getTeam() != currentPlayer) {
//                        allowedMoves = temp.allowedMoves();
//                        if (allowedMoves.contains(king.getCurrentPosition())) {
//                            logger.info("if you move like this you will get yourself in check");
//                            piece.setCurrentPosition(ogPosition);
//                            board[xPiece][yPiece] = piece;
//                            board[xEmpty][yEmpty] = new Empty(Piece.PieceType.EMPTY);
//                            //ZRUS TAH IDK JAK je potreba aby se vubec neprovedl
//                            return true;
//                        }
//                    }
//                }
//            }
//        }
//        //return piece to its actual position
//        piece.setCurrentPosition(ogPosition);
//        board[xPiece][yPiece] = piece;
//        board[xEmpty][yEmpty] = new Empty(Piece.PieceType.EMPTY);
//        return false;
//    }

    public Piece findKing(Team team) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j].getPieceType() == Piece.PieceType.KING && board[i][j].getTeam() == team) {
                    return board[i][j];
                }
            }
        }
        return null;
    }

    //one round
    //TBD BOOLEAN
    public void movePiece(Piece piece, String potentialPosition) {
        isMoved = false;
        InputTransfer position = new InputTransfer();
        Utils convertor = new Utils();
        ArrayList<String> allowedMoves = piece.allowedMoves();
        String curPos = piece.getCurrentPosition();
        String input;
//        staleMate();
        int x;
        int y;
        if (gameActive) {
            //you can make move only if its your turn
            if (getGameState() == piece.getTeam()) { // && !willGetYourselfInCheck(piece, potentialPosition)
                //if not in check
//                if ((getGameState() == Team.BLACK && !blackInCheck) || (getGameState() == Team.WHITE && !whiteInCheck)) {
                //castling
                if (piece.getPieceType() == Piece.PieceType.KING &&
                        position.transfer(board, potentialPosition).getPieceType() == Piece.PieceType.ROOK &&
                        piece.getTeam() == position.transfer(board, potentialPosition).getTeam() &&
                        piece.isFirstMove() &&
                        position.transfer(board, potentialPosition).isFirstMove()) {
                    castling(piece, potentialPosition);
                    logger.info("Castling");
                }
                //classic move
                if (allowedMoves.contains(potentialPosition)) {
                    if (position.transfer(board, potentialPosition).getPieceType() != Piece.PieceType.EMPTY) {
                        if (position.transfer(board, potentialPosition).getPieceType() == Piece.PieceType.KING) {
                            gameActive = false;
                            changeGameState();
                            app.win(getGameState());
                            changeGameState();
                        }
                        //capture Piece
                        else {
                            changeGameState();
                            logger.info("Player " + getGameState() + " lost his piece " + position.transfer(board, potentialPosition).getPieceType());
                            if (position.transfer(board, potentialPosition).getTeam() == Team.WHITE) {
                                capturedPiecesWhite.add(position.transfer(board, potentialPosition));
                            } else {
                                capturedPiecesBlack.add(position.transfer(board, potentialPosition));
                            }
                            willGetOpponentInCheck(piece, potentialPosition);
                            changeGameState();
                            if (order % 2 == 0) {
                                historyOfGame += (order / 2) + 1 + ". ";
                            }
                            if (piece.getPieceType() != Piece.PieceType.PAWN) {
                                historyOfGame += piece.getPieceChar() + "x" + potentialPosition + " ";
                            } else {
                                historyOfGame += curPos.charAt(0) + "x" + potentialPosition + " ";
                            }
                            order++;
                            isMoved = true;
                        }
                    }
                    //GUI promotion
                    x = convertor.getX(potentialPosition);
                    if (piece.getPieceType() == Piece.PieceType.PAWN) {
                        curPos = piece.getCurrentPosition();
                        if (piece.getTeam() == Team.BLACK && x == 0) {
                            input = app.promotion();
                            piece = choosePiece(input, piece, Team.BLACK, curPos);
                            if (order % 2 == 0) {
                                historyOfGame += (order / 2) + 1 + ". ";
                            }
                            historyOfGame += potentialPosition + "=" + piece.getPieceChar() + " ";
                            order++;
                            isMoved = true;
                        } else if (piece.getTeam() == Team.WHITE && x == 7) {
                            input = app.promotion();
                            piece = choosePiece(input, piece, Team.WHITE, curPos);
                            if (order % 2 == 0) {
                                historyOfGame += (order / 2) + 1 + ". ";
                            }
                            historyOfGame += potentialPosition + "=" + piece.getPieceChar() + " ";
                            order++;
                            isMoved = true;
                        }
                    }
                    //console chess promotion
                    //                x = convertor.getX(potentialPosition);
                    //                if (piece.getPieceType() == Piece.PieceType.PAWN) {
                    //                    curPos = piece.getCurrentPosition();
                    //                    Scanner stdin = new Scanner(System.in);
                    //                    if (piece.getTeam() == Team.BLACK && x == 0) {
                    //                        System.out.println("Choose a piece: ");
                    //                        String input = stdin.nextLine();
                    //                        piece = choosePiece(input,piece,Team.BLACK,curPos);
                    //                    } else if (piece.getTeam() == Team.WHITE && x == 7) {
                    //                        System.out.println("Choose a piece: ");
                    //                        String input = stdin.nextLine();
                    //                        piece = choosePiece(input,piece,Team.WHITE,curPos);
                    //                    }
                    //                }
                    x = convertor.getX(piece.getCurrentPosition());
                    y = convertor.getY(piece.getCurrentPosition());
                    board[x][y] = new Empty(Piece.PieceType.EMPTY);
                    piece.setCurrentPosition(potentialPosition);
                    x = convertor.getX(potentialPosition);
                    y = convertor.getY(potentialPosition);
                    board[x][y] = piece;
                    changeEnPassantState();
                    if (piece.isFirstMove()) {
                        if (piece.getPieceType() == Piece.PieceType.PAWN) {
                            x = convertor.getX(potentialPosition);
                            if (x == 3 || x == 4) {
                                piece.setEnPassantReady(true);
                            }
                        }
                        piece.setFirstMove(false);
                    }
                    willGetOpponentInCheck(piece, potentialPosition);
                    changeGameState();
                    if (!isMoved) {
                        if (order % 2 == 0) {
                            historyOfGame += (order / 2) + 1 + ". ";
                        }
                        // TBD check in PGN
                        historyOfGame += piece.getPieceChar() + potentialPosition + " ";
                        if (whiteInCheck || blackInCheck) {
                            whiteInCheck = false;
                            blackInCheck = false;
                            historyOfGame = historyOfGame.strip();
                            historyOfGame += "+" + " ";
                        }
                        //
                        order++;
                        isMoved = true;
                    }
                    // en passant
                } else if (piece.getPieceType() == Piece.PieceType.PAWN) {
                    x = convertor.getX(potentialPosition);
                    y = convertor.getY(potentialPosition);
                    int y2 = convertor.getY(piece.getCurrentPosition());
                    int x2 = convertor.getX(piece.getCurrentPosition());
                    if (y + 1 == y2 || y - 1 == y2 && x + 1 == x2 || x - 1 == x2) {
                        if (piece.getTeam() == Team.BLACK) {
                            x += 2;
                        }
                        if (position.transfer(board, getStringPosition(x, y)).getPieceType() != null &&
                                piece.getPieceType() == Piece.PieceType.PAWN &&
                                position.transfer(board, getStringPosition(x, y)).getPieceType() == Piece.PieceType.PAWN &&
                                piece.getTeam() != position.transfer(board, getStringPosition(x, y)).getTeam() &&
                                position.transfer(board, getStringPosition(x, y)).isEnPassantReady()) {
                            enPassant(piece, potentialPosition);
                            logger.info("En passant");
                        }
                    }
                }
                //move out of check
            }
//            else {
//                    Piece enemy;
//                    ArrayList<Piece> enemies = new ArrayList<>();
//                    Piece king;
//                    if (blackInCheck) {
//                        king = findKing(Team.BLACK);
//                    } else {
//                        king = findKing(Team.WHITE);
//                    }
//                    checkMated(king);
//                    //find who threatens the king
//                    for (int i = 0; i < 8; i++) {
//                        for (int j = 0; j < 8; j++) {
//                            enemy = board[i][j];
//                            if (enemy.getPieceType() != Piece.PieceType.EMPTY) {
//                                if (enemy.allowedMoves() != null && enemy.getTeam() != king.getTeam()) {
//                                    if (enemy.allowedMoves().contains(king.getCurrentPosition())) {
//                                        enemies.add(enemy);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    //more than one piece threatens the king
//                    if (enemies.size() > 1) {
//                        if (piece.getPieceType() == Piece.PieceType.KING) {
//                            if (!willGetYourselfInCheck(piece, potentialPosition)) {
//                                whiteInCheck = false;
//                                blackInCheck = false;
//                                deleteAllowedMoves();
//                                movePiece(piece, potentialPosition);
//                            }
//                            logger.info("still in check");
//                        }
//                        logger.info("you have to move king piece to move out of check");
//                        //only one piece threatens the king
//                    } else {
//                        for (int i = 0; i < 8; i++) {
//                            for (int j = 0; j < 8; j++) {
//                                if (board[i][j].getPieceType() != Piece.PieceType.EMPTY) {
//                                    if (!willGetYourselfInCheck(piece, potentialPosition) && piece.getAllowedMoves() != null && piece.getTeam() == getGameState()) {
//                                        if (piece.getAllowedMoves().contains(potentialPosition)) {
//                                            whiteInCheck = false;
//                                            blackInCheck = false;
//                                            deleteAllowedMoves();
//                                            movePiece(piece, potentialPosition);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
        }
    }
//    //delete pieces allowed moves after getting out of check
//    public void deleteAllowedMoves() {
//        for (int a = 0; a < 8; a++) {
//            for (int b = 0; b < 8; b++) {
//                if (board[a][b].getPieceType() != Piece.PieceType.EMPTY){
//                    board[a][b].setAllowedMoves(null);
//                }
//            }
//        }
//    }
//    //find out moves that will get you out of check and add them to piece
//    public void checkMated(Piece piece) {
//        ArrayList<Piece> myPieces = new ArrayList<>();
//        ArrayList<String> allowedMoves;
//        ArrayList<String> possibleMoves = new ArrayList<>();
//        //find out all pieces of the player
//        for (int i = 0; i < 8; i++) {
//            for (int j = 0; j < 8; j++) {
//                if (board[i][j].getPieceType() != Piece.PieceType.EMPTY){
//                    if (piece.getTeam() == board[i][j].getTeam()) {
//                        myPieces.add(board[i][j]);
//                    }
//                }
//            }
//        }
//        for (Piece each : myPieces) {
//            allowedMoves = each.allowedMoves();
//            //find out which allowed moves will get you out of check
//            for (String allowedMove : allowedMoves) {
//                if (!willGetYourselfInCheck(each, allowedMove)) {
//                    possibleMoves.add(allowedMove);
//                }
//            }
//            each.setAllowedMoves(possibleMoves);
//        }
//    }
//
//    public boolean staleMate() {
//        Piece piece;
//        ArrayList<String> allowedMoves;
//        ArrayList<String> whiteAllowedMoves = new ArrayList<>();
//        ArrayList<String> blackAllowedMoves = new ArrayList<>();
//        for (int i = 0; i < 8; i++) {
//            for (int j = 0; j < 8; j++) {
//                piece = board[i][j];
//                if (piece.getPieceType() != Piece.PieceType.EMPTY) {
//                    allowedMoves = piece.allowedMoves();
//                    if (piece.getTeam() == Team.WHITE) {
//                        for (String allowedMove : allowedMoves) {
//                            whiteAllowedMoves.add(allowedMove);
//                        }
//                    } else {
//                        for (String allowedMove : allowedMoves) {
//                            blackAllowedMoves.add(allowedMove);
//                        }
//                    }
//                }
//            }
//        }
//        //if there are no allowed moves, game has to end
//        if (whiteAllowedMoves == null) {
//            //BILEJ PAT CERNEJ VYHRAL
//            logger.info("Black player has won the game");
//            gameActive = false;
//            return true;
//        } else if (blackAllowedMoves == null) {
//            //CERNEJ PAT BILEJ VYHRAL
//            logger.info("White player has won the game");
//            gameActive = false;
//            return true;
//        }
//        return false;
//    }

    public void castling(Piece piece, String potentialPosition) {
        InputTransfer position = new InputTransfer();
        Utils convertor = new Utils();
        int xRook = convertor.getX(position.transfer(board, potentialPosition).getCurrentPosition());
        int yRook = convertor.getY(position.transfer(board, potentialPosition).getCurrentPosition());
        //kingside castling
        if (yRook == 7 &&
                board[xRook][yRook - 1].getPieceType() == Piece.PieceType.EMPTY &&
                board[xRook][yRook - 2].getPieceType() == Piece.PieceType.EMPTY) {
            board[xRook][yRook - 1] = piece;
            String newPosition = getStringPosition(xRook+1, yRook - 1);
            piece.setCurrentPosition(newPosition);
            board[xRook][yRook - 2] = position.transfer(board, potentialPosition);
            newPosition = getStringPosition(xRook+1, yRook - 2);
            position.transfer(board, potentialPosition).setCurrentPosition(newPosition);
            board[xRook][yRook - 3] = new Empty(Piece.PieceType.EMPTY);
            board[xRook][yRook] = new Empty(Piece.PieceType.EMPTY);
            changeEnPassantState();
            willGetOpponentInCheck(piece, potentialPosition);
            changeGameState();
            if (order % 2 == 0) {
                historyOfGame += (order/2)+1 + ". ";
            }
            historyOfGame += "O-O" + " ";
            order++;
            isMoved = true;
        }
        //queenside castling
        if (yRook == 0 && board[xRook][yRook + 1].getPieceType() == Piece.PieceType.EMPTY &&
                board[xRook][yRook + 2].getPieceType() == Piece.PieceType.EMPTY &&
                board[xRook][yRook + 3].getPieceType() == Piece.PieceType.EMPTY) {
            board[xRook][yRook + 2] = piece;
            String newPosition = getStringPosition(xRook+1, yRook + 1);
            piece.setCurrentPosition(newPosition);
            board[xRook][yRook + 3] = position.transfer(board, potentialPosition);
            newPosition = getStringPosition(xRook+1, yRook + 2);
            position.transfer(board, potentialPosition).setCurrentPosition(newPosition);
            board[xRook][yRook + 1] = new Empty(Piece.PieceType.EMPTY);
            board[xRook][yRook + 4] = new Empty(Piece.PieceType.EMPTY);
            board[xRook][yRook] = new Empty(Piece.PieceType.EMPTY);
            changeEnPassantState();
            willGetOpponentInCheck(piece, potentialPosition);
            changeGameState();
            if (order % 2 == 0) {
                historyOfGame += (order/2)+1 + ". ";
            }
            historyOfGame += "O-O-O" + " ";
            order++;
            isMoved = true;
        }
    }

    public void enPassant(Piece piece, String potentialPosition) {
        InputTransfer position = new InputTransfer();
        Utils convertor = new Utils();
        int x;
        int y;
        x = convertor.getX(piece.getCurrentPosition());
        y = convertor.getY(piece.getCurrentPosition());
        board[x][y] = new Empty(Piece.PieceType.EMPTY);
        piece.setCurrentPosition(potentialPosition);
        x = convertor.getX(potentialPosition);
        y = convertor.getY(potentialPosition);
        board[x][y] = piece;
        if (piece.getTeam() == Team.BLACK) {
            x += 1;
        } else {
            x -= 1;
        }
        board[x][y] = new Empty(Piece.PieceType.EMPTY);
        if (position.transfer(board, potentialPosition).getTeam() == Team.WHITE) {
            capturedPiecesBlack.add(position.transfer(board, potentialPosition));
        } else {
            capturedPiecesWhite.add(position.transfer(board, potentialPosition));
        }
        logger.info("Player " + getGameState() + " lost his piece " + position.transfer(board, potentialPosition).getPieceType());
        changeEnPassantState();
        willGetOpponentInCheck(piece, potentialPosition);
        changeGameState();
        isMoved = true;
    }

    public Piece choosePiece(String input, Piece piece, Team team, String curPos) {
        switch (input) {
            case "b":
                piece = new Bishop(Piece.PieceType.BISHOP, false, team, curPos, null, board);
                break;
            case "k":
                piece = new Knight(Piece.PieceType.KNIGHT, false, team, curPos, null, board);
                break;
            case "q":
                piece = new Queen(Piece.PieceType.QUEEN, false, team, curPos, null, board);
                break;
            case "r":
                piece = new Rook(Piece.PieceType.ROOK, false, team, curPos, null, board);
                break;
        }
        return piece;
    }


    public String position;
    public String getStringPosition(int row, int column) {
        switch (column) {
            case 0:
                position = "a"+row;
                break;
            case 1:
                position = "b"+row;
                break;
            case 2:
                position = "c"+row;
                break;
            case 3:
                position = "d"+row;
                break;
            case 4:
                position = "e"+row;
                break;
            case 5:
                position = "f"+row;
                break;
            case 6:
                position = "g"+row;
                break;
            case 7:
                position = "h"+row;
                break;
        }
        return position;
    }

    public void changeEnPassantState() {
        InputTransfer position = new InputTransfer();
        Piece tempPiece;
        for (int i = 1; i <= 8; i++) {
            for (int j = 0; j < 8; j++) {
                tempPiece = position.transfer(board, getStringPosition(i, j));
                if (tempPiece.getPieceType() == Piece.PieceType.PAWN && tempPiece.isEnPassantReady()) {
                    tempPiece.setEnPassantReady(false);
                }
            }
        }
    }

    public Piece findRandomBlackPiece() {
        ArrayList<Piece> pieces = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j].getTeam() == Team.BLACK) {
                    pieces.add(board[i][j]);
                }
            }
        }
        Random random = new Random();
        Piece piece = pieces.get(random.nextInt(pieces.size()));
        while (piece.allowedMoves().isEmpty()) {
            piece = pieces.get(random.nextInt(pieces.size()));
        }
        return piece;
    }

    public String findRandomAllowedMove(Piece piece) {
        ArrayList<String> allowedMoves = piece.allowedMoves();
        Random random = new Random();
        return allowedMoves.get(random.nextInt(allowedMoves.size()));
    }

    public boolean isMoved() {
        return isMoved;
    }

    public Piece[][] getBoard() {
        return board;
    }

    public String getCapturedPiecesWhite() {
        StringBuffer pieces = new StringBuffer();
        for (Piece piece : capturedPiecesWhite) {
            pieces.append(piece);
            pieces.append(" ");
        }
        return pieces.toString().strip();
    }

    public String getCapturedPiecesBlack() {
        StringBuffer pieces = new StringBuffer();
        for (Piece piece : capturedPiecesBlack) {
            pieces.append(piece);
            pieces.append(" ");
        }
        return pieces.toString().strip();
    }

    public void saveFile(String historyOfGame) {
        //write data
        try {
            Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("pgn.txt"), "UTF8"));
            out.write(historyOfGame);
            out.close();
        } catch (UnsupportedEncodingException e) {
            logger.info("Unsupported encoding");
        } catch (IOException e) {
            logger.info("IOException");
        }
    }

    public boolean isWhiteInCheck() {
        return whiteInCheck;
    }

    public boolean isBlackInCheck() {return blackInCheck;}
}