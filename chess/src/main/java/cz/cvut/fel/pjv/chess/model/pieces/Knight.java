package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public class Knight extends Piece {

    public Knight(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board) {
        super(pieceType, isFirstMove, team, currentPosition, allowedMoves, board);
    }

    @Override
    public ArrayList<String> allowedMoves() {
        ArrayList<String> allowedMoves = new ArrayList<>();
        InputTransfer position = new InputTransfer();

        char letter = currentPosition.charAt(0);
        char tmp = currentPosition.charAt(1);
        int num = Character.getNumericValue(tmp);

//        right up and down
        letter++;
        if (letter != 'i' && letter != 'j') {  //check if move isn't out of board
            potentialPosition = letter + String.valueOf(num + 2);
            if (num + 2 <= BOARD_SIZE) {
                if (position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
            potentialPosition = letter + String.valueOf(num - 2);
            if (num - 2 >= 1) {
                if (position.transfer(board, potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board, potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
        }
        letter++;
        if (letter != 'i' && letter != 'j') {  //check if move isn't out of board
            potentialPosition = letter + String.valueOf(num + 1);
            if (num+1 <= BOARD_SIZE) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }

            potentialPosition = letter + String.valueOf(num - 1);
            if (num-1 >= 1) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
        }

//        left up and down
        letter = currentPosition.charAt(0);
        letter--;
        if (letter != '`' && letter != '_') {  //check if move isn't out of board
            potentialPosition = letter + String.valueOf(num + 2);
            if (num+2 <= BOARD_SIZE) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }

            potentialPosition = letter + String.valueOf(num - 2);
            if (num-2 >= 1) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
        }

        letter--;
        if (letter != '`' && letter != '_') {
            potentialPosition = letter + String.valueOf(num + 1);
            if (num+1 <= BOARD_SIZE) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }

            potentialPosition = letter + String.valueOf(num - 1);
            if (num-1 >= 1) {
                if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                        || this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                }
            }
        }
        return allowedMoves;
    }

    @Override
    public String getPieceChar() {
        return "N";
    }

    @Override
    public String toString() {
        if (team == Team.WHITE) {
            return "\u2658";

        } else {
            return "\u265E" + "\u2009";

        }
    }
}
