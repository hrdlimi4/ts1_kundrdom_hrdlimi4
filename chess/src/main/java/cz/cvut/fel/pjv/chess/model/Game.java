package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.Piece;
import cz.cvut.fel.pjv.chess.view.App;

public class Game {
    public static void main(String[] args) {
        //Board b = new Board(null);
        Board b = new Board((App) null);
        Piece[][] board = b.setUpBoard();
        InputTransfer inputTransfer = new InputTransfer();

        while (true) {
            System.out.println("It is " + b.getGameState() + " turn.");
            inputTransfer.playerInput();
            Piece piece = inputTransfer.inputTransfer(board);
            System.out.println(piece.allowedMoves());
            inputTransfer.playerInput();
            b.movePiece(piece, inputTransfer.input);
            b.printBoard();
        }
    }
}
