package cz.cvut.fel.pjv.chess.view;

import cz.cvut.fel.pjv.chess.model.Clock;
import cz.cvut.fel.pjv.chess.model.Team;
import cz.cvut.fel.pjv.chess.model.Utils;
import cz.cvut.fel.pjv.chess.model.board.Board;
import cz.cvut.fel.pjv.chess.model.pieces.*;
import cz.cvut.fel.pjv.chess.network.Client;
import cz.cvut.fel.pjv.chess.network.Server;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Logger;


public class App extends Application {
    Scene scene, mainMenu;
    Stage primaryStage;
    final static Logger logger = Logger.getLogger(App.class.getName());

    //generate board
    Board b = new Board(this);
    Piece[][] board = b.setUpBoard();
    Piece piece;
    String potentialPosition;
    String currentPosition;
    boolean pieceClicked = false;
    GridPane chessGame = new GridPane();
    Clock clock1 = new Clock();
    Clock clock2 = new Clock();

    BorderPane appLayout = new BorderPane();
    BorderPane chessBoard = new BorderPane();
    GridPane lettersTop = new GridPane();
    GridPane lettersBottom = new GridPane();
    GridPane numbersRight = new GridPane();
    GridPane numbersLeft = new GridPane();
    BorderPane leftMenu = new BorderPane();
    BorderPane blackPlayerUtils = new BorderPane();
    Label gameState = new Label(b.getGameState().name());
    BorderPane whitePlayerUtils = new BorderPane();
    BorderPane rightMenu = new BorderPane();
    Button saveGame = new Button("Save game");
    TableView<History> table = new TableView<>();
    Label capturedPiecesBlack = new Label();
    Label capturedPiecesWhite = new Label();
    TextField nameField;

    ArrayList<String> users = new ArrayList<>();



    GameStateUpdate gameStateUpdate = new GameStateUpdate(gameState, this, b, table, capturedPiecesWhite, capturedPiecesBlack);

    // server
    private static final int PORT_NUMBER = 4444;   // fixed port number and host!
    private static final String HOST = "localhost";

    private Server server;
    private Client client;
    private boolean clientOnly;

    @Override
    public void start(Stage primaryStage) {
        // basic appLayout
        this.primaryStage = primaryStage;


        //main menu with buttons
        mainMenu = mainMenu();

        primaryStage.setTitle("Main menu");
        primaryStage.setScene(mainMenu);
        primaryStage.centerOnScreen();
        primaryStage.show();
    }

    @Override
    public void stop() {
        // 2. close all resources
        if (client != null) {
            client.close();
        }
        if (!clientOnly) {
            if (server != null) {
                server.stop();
            }
        }
    }

    //server
    private void startUpBackend() {
        String team = nameField.getText().strip();
        client = new Client(this, HOST, PORT_NUMBER, team);
        if (!isServerRunning()) {
            server = new Server(client, PORT_NUMBER);
            startServerThenClient();
            clientOnly = false;
        } else {
            startClient();
            clientOnly = true;
        }
    }

    private void showLoginWindow() {
        Label nameLabel = new Label("Enter your name:");
        nameField = new TextField();
        nameField.setOnKeyPressed((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                // 2. start server (if not already running) and client
                startUpBackend();
                showChessBoard();
                printPieces(chessGame);
                onlineGame(chessGame);
                primaryStage.setResizable(false);
                primaryStage.setScene(gameWithoutTime()); // Place in scene in the stage
                primaryStage.centerOnScreen();
                primaryStage.show();
            }
        });
        Button startButton = new Button("Start");
        startButton.setOnAction((ActionEvent e) -> {
            // 2. as above
            startUpBackend();
            showChessBoard();
            printPieces(chessGame);
            onlineGame(chessGame);
            primaryStage.setResizable(false);
            primaryStage.setScene(gameWithoutTime()); // Place in scene in the stage
            primaryStage.centerOnScreen();
            primaryStage.show();
        });
        HBox hbox = new HBox(4, nameLabel, nameField, startButton);
        hbox.setPadding(new Insets(8));
        hbox.setAlignment(Pos.CENTER);
        Scene startScene = new Scene(hbox);
        primaryStage.setScene(startScene);
    }

    private void sendMove() {
        String move = currentPosition + " " + potentialPosition;
        if (b.isMoved()) {
            client.sendMessage(move);
        }
    }

    public void movePiece(Piece piece, String to) {
        if (b.getGameState() == Team.BLACK){
            b.movePiece(piece, to);
            printBoard(chessGame);
        }
    }

    public void showAlert(String msg) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Chess Client");
            alert.setHeaderText(null);
            alert.setContentText(msg);
            alert.show();
        });
    }

    private boolean isServerRunning() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
            serverSocket.close();
            return false;
        } catch (IOException ex) {
            return true;
        }
    }

    private void startServerThenClient() {
        new Thread(server).start();
    }

    private void startClient() {
        new Thread(client).start();
    }

    public void showChessBoard() {
        chessBoard.setCenter(chessGame);
        appLayout.setCenter(chessBoard);

//BOARD
        chessBoard.setPadding(new Insets(0, 10, 0, 10));
        chessGame.setPadding(new Insets(5, 5, 5, 5));

        //top
        //letters
        char letter = 'A';
        for (int i = 0; i < 8; i++) {
            StackPane nav = new StackPane();
            nav.setPrefWidth(80);
            String temp = String.valueOf(letter);
            Label let = new Label(temp);
            let.setFont(new Font(20));
            nav.getChildren().addAll(let);
            letter++;
            lettersTop.add(nav, i, 1);
        }
        lettersTop.setAlignment(Pos.CENTER);
        chessBoard.setTop(lettersTop);

        //bottom
        //letters
        char letterB = 'A';
        for (int i = 0; i < 8; i++) {
            StackPane nav = new StackPane();
            nav.setPrefWidth(80);
            String temp = String.valueOf(letterB);
            Label let = new Label(temp);
            let.setFont(new Font(20));
            nav.getChildren().addAll(let);
            letterB++;
            lettersBottom.add(nav, i, 1);
        }
        chessBoard.setBottom(lettersBottom);
        lettersBottom.setAlignment(Pos.CENTER);

        //right
        //numbers
        for (int i = 8; i > 0; i--) {
            StackPane nav = new StackPane();
            nav.setPrefHeight(80);
            String temp = String.valueOf(9-i);
            Label let = new Label(temp);
            let.setFont(new Font(20));
            nav.getChildren().addAll(let);
            numbersRight.add(nav, 1, i);
        }
        chessBoard.setRight(numbersRight);

        //left
        //numbers
        for (int i = 0; i < 8; i++) {
            StackPane nav = new StackPane();
            nav.setPrefHeight(80);
            String temp = String.valueOf(8-i);
            Label let = new Label(temp);
            let.setFont(new Font(20));
            nav.getChildren().addAll(let);
            numbersLeft.add(nav, 1, i);
        }
        chessBoard.setLeft(numbersLeft);

//APP
        //left
        //menu
        appLayout.setLeft(leftMenu);

        //top
        //black player info

        Label blackPlayer = new Label("Player 2");
        blackPlayerUtils.setLeft(blackPlayer);

        blackPlayerUtils.setCenter(capturedPiecesBlack);
        capturedPiecesBlack.setFont(new Font("Roboto", 20));


        // TODO: list of players
//        Button usersButton = new Button("Show users");
//
//        usersButton.setOnAction(event -> {
//            Popup usersPopup = new Popup();
//            if (users.size() > 0) {
//                for (String user : users) {
//                    Label userLabel = new Label(user);
//                    userLabel.setStyle(" -fx-background-color: white;");
//                    usersPopup.getContent().add(userLabel);
//
//                }
//            } else {
//                Label nothing = new Label("sh");
//                usersPopup.getContent().add(nothing);
//            }
//            usersPopup.setAutoHide(true);
//            Stage popup = new Stage();
//            popup.setTitle("users");
//            usersPopup.show(popup);
//            popup.show();
//        });
//        blackPlayerUtils.setRight(usersButton);


        appLayout.setTop(blackPlayerUtils);

        //game state
        gameState.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        gameState.setStyle("-fx-text-fill: black; -fx-font-size: 16px; -fx-font-weight: bold");
        blackPlayerUtils.setTop(gameState);
//        gameStateUpdate.start();

        //bottom
        //white player info
        Label whitePlayer = new Label("Player 1");
        whitePlayerUtils.setLeft(whitePlayer);

        whitePlayerUtils.setCenter(capturedPiecesWhite);
        capturedPiecesWhite.setFont(new Font("Roboto", 20));

        appLayout.setBottom(whitePlayerUtils);


        //right
        //table
        table.setMaxHeight(640);
        table.setMaxWidth(180);

        TableColumn<History, String> number = new TableColumn<>("number");
        number.setCellValueFactory(new PropertyValueFactory<>("number"));
        TableColumn<History, String> white = new TableColumn<>("white");
        white.setCellValueFactory(new PropertyValueFactory<>("white"));
        TableColumn<History, String> black = new TableColumn<>("black");
        black.setCellValueFactory(new PropertyValueFactory<>("black"));

        table.getColumns().add(number);
        table.getColumns().add(white);
        table.getColumns().add(black);

        number.setResizable(false);
        white.setResizable(false);
        black.setResizable(false);

        number.setPrefWidth(60);
        white.setPrefWidth(60);
        black.setPrefWidth(60);

        appLayout.setRight(rightMenu);
        rightMenu.setPadding(new Insets(0, 15, 0, 0));

        rightMenu.setCenter(table);

        //save button
        saveGame.setOnAction(event -> b.saveFile(b.getHistoryOfGame()));

        whitePlayerUtils.setRight(saveGame);

        printBoard(chessGame);
    }

    //timer
    private void startWhiteTimer(Label timer) {
        ClockUpdate clockUpdate = new ClockUpdate(timer, clock1, this, b);
        clock1.start();
        clockUpdate.start();
        logger.info("White timer started.");
    }

    private void startBlackTimer(Label timer) {
        ClockUpdate clockUpdate = new ClockUpdate(timer, clock2, this, b);
        clock2.start();
        clockUpdate.start();
        logger.info("Black timer started.");
    }

    private void stopTimer() {
        if (clock2.isStopped() && b.isMoved()) {
            clock1.stop();
            clock2.start();
        } else if (clock1.isStopped() && b.isMoved()) {
            clock1.start();
            clock2.stop();
        }
    }

    private Scene gameWithoutTime() {
        primaryStage.setResizable(false);
        primaryStage.setTitle("Chess");
        primaryStage.setScene(scene); // Place in scene in the stage
        primaryStage.show();
        return new Scene(appLayout);
    }

    private Scene gameWithTime() {
        Label whiteTimer = new Label();
        whiteTimer.setFont(new Font("Roboto",30));
        rightMenu.setBottom(whiteTimer);
        startWhiteTimer(whiteTimer);

        Label blackTimer = new Label();
        blackTimer.setFont(new Font("Roboto",30));
        rightMenu.setTop(blackTimer);
        startBlackTimer(blackTimer);
        clock2.stop();


        primaryStage.setResizable(false);
        primaryStage.setTitle("Chess");
        primaryStage.setScene(scene); // Place in scene in the stage
        primaryStage.show();
        return new Scene(appLayout);
    }


    private Scene mainMenu() {
        Label label1 = new Label("Select game type:");
        // Takes us to PvP game without time
        Button button1 = new Button("PvP no time limit");
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showChessBoard();
                PvPLocal(chessGame);
                primaryStage.setResizable(false);
                primaryStage.setScene(gameWithoutTime()); // Place in scene in the stage
                primaryStage.centerOnScreen();
                primaryStage.show();
            }
        });
        // Takes us to PvB game without time
        Button button2 = new Button("PvB no time limit");
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showChessBoard();
                PvBLocal(chessGame);
                primaryStage.setResizable(false);
                primaryStage.setScene(gameWithoutTime()); // Place in scene in the stage
                primaryStage.centerOnScreen();
                primaryStage.show();
            }
        });
        // Takes us to PvP game with time
        Button button3 = new Button("PvP with time limit");
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                showChessBoard();
                PvPLocal(chessGame);
                primaryStage.setResizable(false);
                primaryStage.setScene(gameWithTime()); // Place in scene in the stage
                primaryStage.centerOnScreen();
                primaryStage.show();
            }
        });

        // Take us to Online Pvp game
        Button button4 = new Button("Online PvP with no time limit");
        button4.setOnAction(event -> {
            showLoginWindow();
            primaryStage.centerOnScreen();
            primaryStage.show();
        });

        // Takes us to Custom board set up
        Button button5 = new Button("Custom board set up");
        button5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                showChessBoard();
                printEmptyBoard(chessGame);
                userBoardSetUp(chessGame);
                primaryStage.setResizable(false);
                primaryStage.setScene(gameWithoutTime()); // Place in scene in the stage
                primaryStage.centerOnScreen();
                primaryStage.show();
            }
        });

        VBox root1 = new VBox();
        root1.setAlignment(Pos.CENTER);
        root1.getChildren().addAll(label1, button1, button2, button3,button4, button5);
        return new Scene(root1,300,200);
    }


    // Player vs. Player local game
    public void PvPLocal(GridPane pane) {
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            gameStateUpdate.handle();
            printBoard(pane);
            if (!pieceClicked) {
                int coordA = (int) mouseEvent.getSceneX() - 13;
                int coordB = (int) mouseEvent.getSceneY() - 66;
                int x = (coordA / 80);
                int y = (coordB / 80);
                piece = board[7 - y][x];
                if (piece.getTeam() == b.getGameState()) {
                    pieceClicked = true;
                }
                Utils transfer = new Utils();
                Image dot = new Image("dot.png", 80, 80, true, true);
                if (piece.getPieceType() != Piece.PieceType.EMPTY && piece.getTeam() == b.getGameState()) {
                    Image selected = new Image("selected.png", 80, 80, true, true);
                    ImageView selectedImageView = new ImageView(selected);
                    pane.add(selectedImageView, x, y);
                }
                if (piece.getPieceType() != Piece.PieceType.EMPTY && b.getGameState() == piece.getTeam() && b.isGameActive()) {
                    for (String pos : piece.allowedMoves()) {
                        ImageView dotImageView = new ImageView(dot);
                        pane.add(dotImageView, transfer.getY(pos), 7 - transfer.getX(pos));
                    }
                    logger.info("Piece " + piece + " from " + b.getStringPosition(7 - y + 1, x) + " can go to " + piece.allowedMoves());
                }
                gameStateUpdate.handle();
            } else {
                pieceClicked = false;
                int coordA = (int) mouseEvent.getSceneX()- 13;
                int coordB = (int) mouseEvent.getSceneY()- 66;
                int x = (coordA/80);
                int y = (coordB/80);
                String from = piece.getCurrentPosition();
                potentialPosition = b.getStringPosition(7 - y + 1,x);
                b.movePiece(piece, potentialPosition);
                stopTimer();
                if(b.isMoved()) {
                    logger.info(piece + " moved from " + from + " to " + potentialPosition);
                } else {
                    logger.info("Cannot go there");
                }
                printBoard(pane);
                gameStateUpdate.handle();

            }
        });
    }

    // Player vs. Random Bot local game
    public void PvBLocal(GridPane pane) {
        gameStateUpdate.handle();
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (b.getGameState() == Team.WHITE) {
                printBoard(pane);
                if (!pieceClicked) {
                    int coordA = (int) mouseEvent.getSceneX() - 13;
                    int coordB = (int) mouseEvent.getSceneY() - 66;
                    int x = (coordA / 80);
                    int y = (coordB / 80);
                    piece = board[7 - y][x];
                    if (piece.getTeam() == b.getGameState()) {
                        pieceClicked = true;
                    }
                    Utils transfer = new Utils();
                    Image dot = new Image("dot.png", 80, 80, true, true);
                    if (piece.getPieceType() != Piece.PieceType.EMPTY && piece.getTeam() == b.getGameState()) {
                        Image selected = new Image("selected.png", 80, 80, true, true);
                        ImageView selectedImageView = new ImageView(selected);
                        pane.add(selectedImageView, x, y);
                    }
                    if (piece.getPieceType() != Piece.PieceType.EMPTY && b.getGameState() == piece.getTeam() && b.isGameActive()) {
                        for (String pos : piece.allowedMoves()) {
                            ImageView dotImageView = new ImageView(dot);
                            pane.add(dotImageView, transfer.getY(pos), 7 - transfer.getX(pos));
                        }
                        logger.info("Piece " + piece + " from " + b.getStringPosition(7 - y + 1, x) + " can go to " + piece.allowedMoves());

                    }
                    gameStateUpdate.handle();
                } else {
                    pieceClicked = false;
                    int coordA = (int) mouseEvent.getSceneX() - 13;
                    int coordB = (int) mouseEvent.getSceneY() - 66;
                    int x = (coordA / 80);
                    int y = (coordB / 80);
                    String from = piece.getCurrentPosition();
                    potentialPosition = b.getStringPosition(7 - y + 1, x);
                    b.movePiece(piece, potentialPosition);
                    if (b.isMoved()) {
                        logger.info(piece + " moved from " + from + " to " + potentialPosition);
                    } else {
                        logger.info("Cannot go there");
                    }
                    printBoard(pane);
                    Piece randomBlackPiece = b.findRandomBlackPiece();
                    from = randomBlackPiece.getCurrentPosition();
                    String randomAllowedMove = b.findRandomAllowedMove(randomBlackPiece);
                    b.movePiece(randomBlackPiece, randomAllowedMove);
                    logger.info(randomBlackPiece + " moved from " + from + " to " + randomAllowedMove);
                    printBoard(pane);
                    gameStateUpdate.handle();

                }
            }
        });
    }

    // online game
    public void onlineGame(GridPane pane) {
        gameStateUpdate.handle();
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
            if (b.getGameState() == Team.WHITE) {
                printBoard(pane);
                if (!pieceClicked) {
                    int coordA = (int) mouseEvent.getSceneX() - 13;
                    int coordB = (int) mouseEvent.getSceneY() - 66;
                    int x = (coordA / 80);
                    int y = (coordB / 80);
                    piece = board[7 - y][x];
                    currentPosition = piece.getCurrentPosition();
                    if (piece.getTeam() == b.getGameState()) {
                        pieceClicked = true;
                    }
                    Utils transfer = new Utils();
                    Image dot = new Image("dot.png", 80, 80, true, true);
                    if (piece.getPieceType() != Piece.PieceType.EMPTY && piece.getTeam() == b.getGameState()) {
                        Image selected = new Image("selected.png", 80, 80, true, true);
                        ImageView selectedImageView = new ImageView(selected);
                        pane.add(selectedImageView, x, y);
                    }
                    if (piece.getPieceType() != Piece.PieceType.EMPTY && b.getGameState() == piece.getTeam() && b.isGameActive()) {
                        for (String pos : piece.allowedMoves()) {
                            ImageView dotImageView = new ImageView(dot);
                            pane.add(dotImageView, transfer.getY(pos), 7 - transfer.getX(pos));
                        }
                        logger.info("Piece " + piece + " from " + b.getStringPosition(7 - y + 1, x) + " can go to " + piece.allowedMoves());

                    }
                    gameStateUpdate.handle();
                } else {
                    pieceClicked = false;
                    int coordA = (int) mouseEvent.getSceneX() - 13;
                    int coordB = (int) mouseEvent.getSceneY() - 66;
                    int x = (coordA / 80);
                    int y = (coordB / 80);
                    String from = piece.getCurrentPosition();
                    potentialPosition = b.getStringPosition(7 - y + 1, x);
                    b.movePiece(piece, potentialPosition);
                    if (b.isMoved()) {
                        sendMove();
                        logger.info(piece + " moved from " + from + " to " + potentialPosition);
                    } else {
                        logger.info("Cannot go there");
                    }
                    printBoard(pane);
                    gameStateUpdate.handle();

                }
            }
        });
    }

    //event handler for custom board set up
    EventHandler<MouseEvent> paneSetupClickHandler = (event)-> {
        printBoard(chessGame);
        int coordA = (int) event.getSceneX() - 13;
        int coordB = (int) event.getSceneY() - 66;
        int x = (coordA / 80);
        int y = (coordB / 80);

        String currentPosition = b.getStringPosition(7 - y +1, x);

        Alert promotion = new Alert(Alert.AlertType.CONFIRMATION);
        promotion.setTitle("Choose a piece");
        promotion.setHeaderText("Which piece do you want to place here?");
        promotion.setContentText("Choose your option.");


        ButtonType pawnB = new ButtonType("Black pawn");
        ButtonType kingB = new ButtonType("Black king");
        ButtonType queenB = new ButtonType("Black queen");
        ButtonType knightB = new ButtonType("Black knight");
        ButtonType rookB = new ButtonType("Black rook");
        ButtonType bishopB = new ButtonType("Black bishop");

        ButtonType pawnW = new ButtonType("White pawn");
        ButtonType kingW = new ButtonType("White king");
        ButtonType queenW = new ButtonType("White queen");
        ButtonType knightW = new ButtonType("White knight");
        ButtonType rookW = new ButtonType("White rook");
        ButtonType bishopW = new ButtonType("White bishop");

        ButtonType empty = new ButtonType("Empty");



        promotion.getButtonTypes().setAll(pawnB, kingB, queenB, knightB, rookB, bishopB, pawnW, kingW, queenW, knightW, rookW, bishopW, empty);


        Optional<ButtonType> result = promotion.showAndWait();
        if (result.get() == queenB) {
            piece = new Queen(Piece.PieceType.QUEEN, true, Team.BLACK, currentPosition, null, board);
        } else if (result.get() == queenW) {
            piece = new Queen(Piece.PieceType.QUEEN, true, Team.WHITE, currentPosition, null, board);
        } else if (result.get() == pawnB) {
            piece = new Pawn(Piece.PieceType.PAWN, true, Team.BLACK, currentPosition, null, board, false);
        } else if (result.get() == pawnW) {
            piece = new Pawn(Piece.PieceType.PAWN, true, Team.WHITE, currentPosition, null, board, false);
        } else if (result.get() == kingB) {
            piece = new King(Piece.PieceType.KING, true, Team.BLACK, currentPosition, null, board);
        } else if (result.get() == kingW) {
            piece = new King(Piece.PieceType.KING, true, Team.WHITE, currentPosition, null, board);
        } else if (result.get() == knightB) {
            piece = new Knight(Piece.PieceType.KNIGHT, true, Team.BLACK, currentPosition, null, board);
        } else if (result.get() == knightW) {
            piece = new Knight(Piece.PieceType.KNIGHT, true, Team.WHITE, currentPosition, null, board);
        } else if (result.get() == rookB) {
            piece = new Rook(Piece.PieceType.ROOK, true, Team.BLACK, currentPosition, null, board);
        } else if (result.get() == rookW) {
            piece = new Rook(Piece.PieceType.ROOK, true, Team.WHITE, currentPosition, null, board);
        } else if (result.get() == bishopB) {
            piece = new Bishop(Piece.PieceType.BISHOP, true, Team.BLACK, currentPosition, null, board);
        } else if (result.get() == bishopW) {
            piece = new Bishop(Piece.PieceType.BISHOP, true, Team.WHITE, currentPosition, null, board);
        } else if (result.get() == empty) {
            piece = new Empty(Piece.PieceType.EMPTY);
        }


        board[7 - y][x] = piece;
        printBoard(chessGame);

        logger.info(piece.getTeam() + " " + piece.getPieceType() + " has been placed");
    };


    public void userBoardSetUp(GridPane pane) {
        Button start = new Button("Start game");
        rightMenu.setBottom(start);

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = new Empty(Piece.PieceType.EMPTY);
            }
        }

        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, paneSetupClickHandler);

        start.setOnAction((event)->{
            // TODO: // validate setup - check two kings at least
            pane.removeEventHandler(MouseEvent.MOUSE_CLICKED, paneSetupClickHandler);
            PvPLocal(chessGame);
        });
    }



    public void printEmptyBoard(GridPane pane) {
        // Create board
        int count = 0;
        double s = 80; // side of rectangle
        for (int i = 0; i < 8; i++) {
            count++;
            for (int j = 0; j < 8; j++) {
                Rectangle rectangle = new Rectangle(s, s, s, s);
                if (count % 2 == 0) {
                    rectangle.setFill(Color.rgb(70, 60, 50));
                } else {
                    rectangle.setFill(Color.rgb(220, 220, 210));
                }
                pane.add(rectangle, j, i);
                count++;
            }
        }

    }

    public void printPieces(GridPane pane) {
        //find and place picture of the piece object
        for (int i = 7; i >= 0; i--) {
            for (int j = 7; j >= 0; j--) {
                ImageLoader imageLoader = new ImageLoader();
                imageLoader.loadImage(board, pane, i, j);
            }
        }
    }

    public void printBoard(GridPane pane) {
        printEmptyBoard(pane);
        printPieces(pane);
    }

    //winning dialog
    public void win(Team team) {
        Alert win = new Alert(Alert.AlertType.INFORMATION);

        win.setTitle("Winning dialog");
        if (team == Team.WHITE) {
            team = Team.BLACK;
        } else {
            team = Team.WHITE;
        }
        win.setHeaderText(team + " WINS!");
        win.show();
    }

    //promotion dialog
    public String promotion() {
        String input = "";

        Alert promotion = new Alert(Alert.AlertType.CONFIRMATION);
        promotion.setTitle("Choose a piece");
        promotion.setHeaderText("You reached the end of the board, you can replace your pawn now.");
        promotion.setContentText("Choose your option.");

        ButtonType queen = new ButtonType("Queen");
        ButtonType knight = new ButtonType("Knight");
        ButtonType rook = new ButtonType("Rook");
        ButtonType bishop = new ButtonType("Bishop");

        promotion.getButtonTypes().setAll(queen, knight, rook, bishop);

        Optional<ButtonType> result = promotion.showAndWait();
        if (result.get() == queen){
            input = "q";
        } else if (result.get() == knight) {
            input = "k";
        } else if (result.get() == rook) {
            input = "r";
        } else if (result.get() == bishop) {
            input = "b";
        }
        logger.info("Pawn has been promoted.");
        return input;
    }

    public Piece[][] getBoard() {
        return board;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<String> users) {
        this.users = users;
    }

    public static void main(String[] args) {
        launch();
    }

    public BorderPane getWhitePlayerUtils() {
        return whitePlayerUtils;
    }

    public void setWhitePlayerUtils(BorderPane whitePlayerUtils) {
        this.whitePlayerUtils = whitePlayerUtils;
    }
}