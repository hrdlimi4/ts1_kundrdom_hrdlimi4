package cz.cvut.fel.pjv.chess.view;

import cz.cvut.fel.pjv.chess.model.Clock;
import cz.cvut.fel.pjv.chess.model.board.Board;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Label;

public class ClockUpdate extends AnimationTimer {
    private final Label timer;
    private final Clock clock;
    private final App app;
    private final Board board;
    private long prevTick = 0;

    public ClockUpdate(Label timer, Clock clock, App app, Board board) {
        this.timer = timer;
        this.clock = clock;
        this.app = app;
        this.board = board;
    }
    @Override
    public void handle(long l) {
        if (l - prevTick > 100_000_000) { // each 0.1 sec
            long DEFAULT_TIME = 600_100; // 600 100 milliseconds = 10 minutes with a small reserve
            long second = ((DEFAULT_TIME - clock.getTime()) / 1000) % 60;
            long minute = ((DEFAULT_TIME - clock.getTime()) / (1000 * 60)) % 60;
            String time = String.format("%02d:%02d", minute, second);
            timer.setText(String.valueOf(time));
            if (second == 0.000001) { // reserve
                stop();
                app.win(board.getGameState());
                board.setGameActive(false);
            }
            prevTick = l;
        }
    }
}