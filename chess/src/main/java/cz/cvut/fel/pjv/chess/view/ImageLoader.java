package cz.cvut.fel.pjv.chess.view;

import cz.cvut.fel.pjv.chess.model.pieces.Piece;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.util.logging.Logger;

public class ImageLoader {
    final static Logger logger = Logger.getLogger(ImageLoader.class.getName());

    // If an image is not found throws an Error
    public void loadImage(Piece[][] board, GridPane pane, int i, int j) {
        String imageString = board[7 - i][j].getImage();
        try {
            if (!imageString.equals("null_EMPTY.png")) {
                Image image = new Image(imageString, 80, 80, true, true);
                pane.add(new ImageView(image), j, i);
            }
        } catch (Exception e) {
            logger.warning("File " + imageString + " not found." + e);
        }
    }
}