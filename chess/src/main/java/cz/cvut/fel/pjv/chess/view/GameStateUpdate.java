package cz.cvut.fel.pjv.chess.view;

import cz.cvut.fel.pjv.chess.model.Team;
import cz.cvut.fel.pjv.chess.model.board.Board;
import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;

import java.util.ArrayList;

public class GameStateUpdate {

    private final Label gameState;
    private final App app;
    private final Board board;
    private final TableView table;
    private final Label capturedPiecesBlack;
    private final Label capturedPiecesWhite;


    public GameStateUpdate(Label gameState, App app, Board board, TableView table, Label capturedPiecesBlack, Label capturedPiecesWhite) {
        this.gameState = gameState;
        this.app = app;
        this.board = board;
        this.table = table;
        this.capturedPiecesBlack = capturedPiecesBlack;
        this.capturedPiecesWhite = capturedPiecesWhite;
    }

    Team lastGameState = Team.WHITE;
    ObservableList<History> data = FXCollections.observableArrayList();
    ArrayList<String> temp = new ArrayList<>();



    public void handle() {
        if (board.getGameState() != lastGameState) {
            lastGameState = board.getGameState();
            String newState = String.valueOf(lastGameState);
            gameState.setText(newState);

            capturedPiecesBlack.setText(board.getCapturedPiecesBlack().toString());
            capturedPiecesWhite.setText(board.getCapturedPiecesWhite().toString());


            String historyToParse = board.getHistoryOfGame();
            String[] parsedHistory = historyToParse.split(" ");

             if (parsedHistory.length % 3 == 0) {
                data.clear();
                for (String parse : parsedHistory) {
                    temp.add(parse);
                    if (temp.size() == 3) {
                        History tmp = new History(temp.get(0), temp.get(1), temp.get(2));
                        data.add(tmp);
                        temp.clear();
                    }
                }
                table.setItems(data);
             }
        }
    }
}
