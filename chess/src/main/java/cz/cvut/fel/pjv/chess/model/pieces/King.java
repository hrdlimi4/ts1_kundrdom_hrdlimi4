package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public class King extends Piece {

   public King(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board) {
      super(pieceType, isFirstMove, team, currentPosition, allowedMoves, board);
   }

   @Override
   public ArrayList<String> allowedMoves() {
      ArrayList<String> allowedMoves = new ArrayList<>();
      InputTransfer position = new InputTransfer();

      char letter = currentPosition.charAt(0);
      char tmp = currentPosition.charAt(1);
      int num = Character.getNumericValue(tmp);

      potentialPosition = letter + String.valueOf(num + 1);
      if (num+1 <= BOARD_SIZE) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      potentialPosition = letter + String.valueOf(num - 1);
      if (num-1 >= 1) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      letter++;
      potentialPosition = letter + String.valueOf(num);
      if (letter != 'i') {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      potentialPosition = letter + String.valueOf(num + 1);
      if (letter != 'i' && num+1 <= BOARD_SIZE ) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      potentialPosition = letter + String.valueOf(num - 1);
      if (letter != 'i' && num-1 >= 1 ) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      letter -= 2;
      potentialPosition = letter + String.valueOf(num);
      if (letter != '`' ) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      potentialPosition = letter + String.valueOf(num + 1);
      if (letter != '`' && num+1 <= BOARD_SIZE ) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }

      potentialPosition = letter + String.valueOf(num - 1);
      if (letter != '`' && num-1 >= 1 ) {
         if (position.transfer(board,potentialPosition).pieceType == PieceType.EMPTY
                 || this.team != position.transfer(board,potentialPosition).team) {
            allowedMoves.add(potentialPosition);
         }
      }
      return allowedMoves;
   }

   @Override
   public String getPieceChar() {
      return "K";
   }

   @Override
   public String toString() {
      if (team == Team.WHITE) {
         return "\u2009" + "\u2654" + "\u2009";

      } else {
         return "\u2009" + "\u265A" + "\u2009";

      }
   }

}
