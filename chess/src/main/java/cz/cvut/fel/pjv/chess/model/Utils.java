package cz.cvut.fel.pjv.chess.model;

public class Utils {

//get Y coordinate from standard chess notation
    public int getY(String position) {
        char letter = position.charAt(0);
        int numberFromLetter = letter - 97; // ASCII code for letter 'a' is 97 minus 97 is 0 and so on

        return numberFromLetter;
    }

//get X coordinate from standard chess notation
    public int getX(String position) {
        char tmp = position.charAt(1);
        int numberPosition = Character.getNumericValue(tmp);

        return numberPosition-1;
    }

}
