package cz.cvut.fel.pjv.chess.model.pieces;

import cz.cvut.fel.pjv.chess.model.InputTransfer;
import cz.cvut.fel.pjv.chess.model.Team;

import java.util.ArrayList;


public class Rook extends Piece {

    public Rook(PieceType pieceType, boolean isFirstMove, Team team, String currentPosition, ArrayList<String> allowedMoves, Piece[][] board) {
        super(pieceType, isFirstMove, team, currentPosition, allowedMoves, board);
    }

    @Override
    public ArrayList<String> allowedMoves() {
        ArrayList<String> allowedMoves = new ArrayList<>();
        InputTransfer position = new InputTransfer();

        char letter = currentPosition.charAt(0);
        char tmp = currentPosition.charAt(1);
        int num = Character.getNumericValue(tmp);

        for (int i = num+1; i <= BOARD_SIZE; i++) {
            potentialPosition = letter + String.valueOf(i);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        for (int i = num-1; i >= 1; i--) {
            potentialPosition = letter + String.valueOf(i);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        while (letter != 'h') {
            letter++;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }

        letter = currentPosition.charAt(0);
        while (letter != 'a') {
            letter--;
            potentialPosition = letter + String.valueOf(num);
            if (position.transfer(board,potentialPosition).pieceType != PieceType.EMPTY) {
                if (this.team != position.transfer(board,potentialPosition).team) {
                    allowedMoves.add(potentialPosition);
                    break;
                }
                break;
            }
            allowedMoves.add(potentialPosition);
        }
        return allowedMoves;
    }

    @Override
    public String getPieceChar() {
        return "R";
    }

    @Override
    public String toString() {
        if (team == Team.WHITE) {
            return "\u2656";
        } else {
            return "\u265C";

        }
    }
}
