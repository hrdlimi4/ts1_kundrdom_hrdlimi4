package cz.cvut.fel.pjv.chess.view;

import javafx.beans.property.SimpleStringProperty;

public class History {
    private String number;
    private String white;
    private String black;

    public History() {
        this.number = "";
        this.white = "";
        this.black = "";
    }

    public History(String number, String white, String black) {
        this.number = number;
        this.white = white;
        this.black = black;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getWhite() {
        return white;
    }

    public void setWhite(String white) {
        this.white = white;
    }

    public String getBlack() {
        return black;
    }

    public void setBlack(String black) {
        this.black = black;
    }
}
