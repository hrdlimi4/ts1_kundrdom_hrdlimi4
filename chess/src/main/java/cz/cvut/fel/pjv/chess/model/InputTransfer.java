package cz.cvut.fel.pjv.chess.model;

import cz.cvut.fel.pjv.chess.model.pieces.Piece;

import java.util.Scanner;

public class InputTransfer {
    Scanner stdin;
    String input;

    public void playerInput() {
        stdin = new Scanner(System.in);
        System.out.println("Choose a position: ");
        input = stdin.nextLine();
    }

//finds piece from exact board position
    public Piece inputTransfer(Piece[][] board) {
        char letter = input.charAt(0);
        char tmp = input.charAt(1);
        int numberPosition = Character.getNumericValue(tmp);
        int numberFromLetter = letter - 97; // ASCII code for letter 'a' is 97 minus 97 is 0 and so on

        Piece piece = board[numberPosition-1][numberFromLetter];

        return piece;
    }

//finds a piece according to the code of conventional chess notation
    public Piece transfer(Piece[][] board, String input) {
        char letter = input.charAt(0);
        char tmp = input.charAt(1);
        int numberPosition = Character.getNumericValue(tmp);
        int numberFromLetter = letter - 97; // ASCII code for letter 'a' is 97 minus 97 is 0 and so on

        Piece piece = board[numberPosition-1][numberFromLetter];
        return piece;
    }
}
