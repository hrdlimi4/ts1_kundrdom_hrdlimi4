package cz.cvut.fel.pjv.chess.model.pieces;

import java.util.ArrayList;

public class Empty extends Piece {
    public Empty(PieceType pieceType) {
        super(pieceType);
    }

    @Override
    public ArrayList<String> allowedMoves() {
        return null;
    }

    @Override
    public String toString() {
        return "- " ;
    }
}
