module cz.cvut.fel.pjv.chess {
    requires javafx.controls;
    requires java.logging;
    exports cz.cvut.fel.pjv.chess.view;
}
